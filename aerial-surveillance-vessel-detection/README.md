# Workflow for generating vessel detections from Conservation & Protection Aerial Surveillance Program (C&P ASP) flyovers in R 

__Main authors:__  Lily Burke   
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: lily.burke@dfo-mpo.gc.ca

- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
  + [Software](#software)
  + [Directory Structure](#directory-structure)
- [Acknowledgements](#acknowledgements)
- [References](#references)

## Objective

An automated workflow for generating C&P ASP vessel detection observations from flyover reports

## Summary

Vessel tracking data shed light on a variety of human pressures within marine conservation areas and are valuable for evaluating the effectiveness of vessel- and fishing-related regulations. Aerial surveillance has been conducted by Conservation & Protection (C&P) since 2002 as part of the Aerial Surveillance Program (ASP) to enforce DFO regulated spatial fishing closures. Burke et al. (2022) compiled and analyzed C&P ASP flyover data to demonstrate how these data can be used for human pressure monitoring and management effectiveness evaluation in marine conservation areas. The R scripts found in this project were developed to produce a C&P ASP vessel detection dataset. 

C&P ASP vessel detections: We accessed vessel detection information from multiple reports that were stored in the Surveillance Information Server 3 database (now the AIMS-C4 database). These reports include information on the activities vessels were engaged in, vessel identification (name and unique Maritime Mobile Service Identity number [MMSI], if available), vessel location (latitude/longitude), the speed and course of vessel travel, and the location and timing of the flyovers. We manually downloaded six reports (.csv and .xls files) for each flyover mission that are labeled as: 
  *  MissionTargetsList.csv, 
  *  FishingVesselManualOnTop.xls, 
  *  FishingVesselAis.xls, 
  *  CommercialVesselAis.xls, 
  *  Target.xls, and 
  *  AircraftTracksList.csv 

## Status

Ongoing-improvements

## Contents

All scripts described below run from the ~/code working directory. Other files in directory are sourced in the scripts described below and shown in the Directory Structure. 

**CPaerialsurveillance1_datapreparation.R**
  *  Adds Mission date to the C&P ASP report file name to filter vessel and mission event information in reports from same flyover. 

**CPaerialsurveillance2_vesseldetections.R**    
  *  Extracts and then aggregates vessel location information and mission events from C&P ASP reports

**CPaerialsurveillance3_aircrafttracks.R**    
  *  Extracts C&P ASP flyover information from the aircraft tracks report.

## Methods

**Data Preparation**    
*  Loops through the C&P ASP reports and extracts the Mission date (YYYY-MM-DD) the flyover took place on using CPaerialsurveillance1_datapreparation.R
*  Renames the C&P ASP report file name by pasting the Mission date to the file name. The Mission date in the file name is used to extract vessel and mission event information from same flyover in CPaerialsurveillance2_vesseldetections.R 

**Vessel Detections**    
*  Loops through the C&P ASP reports that have the same Mission date. 
*  Extracts vessel and mission event information from the reports. 
*  Joins vessel and mission event information from the reports using a unique ID. Every vessel detection and mission event have a unique ID and this ID is consistent between reports. 
*  Cleans up data 
*  Writes .csv of vessel detections

**Aircraft tracks**
*  Cleans up C&P ASP flyover information from the aircraft tracks report
*  Writes a .csv of flyover tracks

## Requirements

### Software
* R version 4.0.2 or newer (https://www.r-project.org/)

### Directory Structure

```
/---<CP_ASP_flyovers project-name>   
    /---code   
    |  	  CPaerialsurveillance2_vesseldetections.R
    | 	  CPaerialsurveillance2_vesseldetections.R
    |     CPaerialsurveillance3_aircrafttracks.R
    | 	       
    /---data
    |
    /---reports 
    |	  MissionTargetsList.csv
    |     FishingVesselManualOnTop.xls
    |     FishingVesselAis.xls 
    |     CommercialVesselAis.xls
    |     Target.xls 
    |     AircraftTracksList.csv 
	
   
```

## Acknowledgements

The following individuals: Georgia Clyde, Josephine Iacarella, Emily Rubidge, B. Thexton, J. Prior, Joe Knight. 

## References

Burke, L., Clyde, G., Proudfoot, B., Rubidge, E.M., and Iacarella, J.C. 2022. Monitoring Pacific marine conservation area effectiveness using aerial and RADARSAT-2 (Synthetic Aperture Radar) vessel detection. Can. Tech. Rep. Fish. Aquat. Sci. 3479: xi + 50 p.

Iacarella, J.C., Clyde, G., Dunham, A., 2020. Vessel Tracking Datasets For Monitoring Canada’s Conservation Effectiveness. Can. Tech. Rep. Fish. Aquat. Sci. 3387.
