###############################################################################
#
# Author:       Lily Burke
# Affiliation:  Fisheries and Oceans Canada (DFO)
# Group:        Marine Spatial Ecology and Analysis
# Location:     Institute of Ocean Sciences
# Contact:      lily.burke@dfo-mpo.gc.ca
#
# Overview: 
# Step 3 of cleaning up aircraft tracks report from the Conservation & 
# Protection Aerial Surveillance Program (C&P ASP) flyovers.
#
# Loops through the C&P ASP aircraft tracks report that has the location 
# (latitude/longitude), altitude, date and time of the flyover track. 
#
#Requirements:
# i) aircraft tracks files are saved in reports folder.
#
# Troubleshooting:
# * Compare the report names to the filenames used in the code below to read 
# in the reports and make sure they match
# * In the code that cleans up the latitude/longitude columns the degree symbol 
# is sometimes replaced by a question mark or a zero. Confirm that the correct
# character is removed prior to conversion to decimal degrees.
# 
# Citation: Burke, L., Clyde, G., Proudfoot, B., Rubidge, E.M., and 
# Iacarella, J.C. 2022. Monitoring Pacific marine conservation area 
# effectiveness using aerial and RADARSAT-2 (Synthetic Aperture Radar)
# vessel detection. Can. Tech. Rep. Fish. Aquat. Sci.3479: xi + 50 p.
#
################################################################################


# ----- Load required libraries ------------------------------------------------
library(dplyr)


# Loop  through aircraft tracks report by flyover date ------------------------- 
dirs <- list.dirs("G:/CP_ASP_flyovers/reports")
files <- list.files(dirs)
file_dates <- unique(stringr::str_extract(files, "^.{10}"))
year <- unique(paste0(substring(file_dates, 1,4)))

for (f in file_dates) {
  files_year <- list.files(dirs, pattern = f, full.names = TRUE)
  
  # Get the Mission Number from the Target report
  cat("Read in Target report: ", f)
  target.df <- read_excel(files_year[grepl("*Target20", files_year)])

  # Get Mission information from Target report to add to df later
  mission <- target.df[4,1]
  mission <- gsub(".*: ","", mission)
  mission.number <- str_sub(mission, start= -3)
  print(mission)


  # Report 6: Aircraft Tracks report--------------------------------------------
  cat("Read in Mission Target List report: ", f)
  df <- read.csv(files_year[grepl("*AircraftTracks*", files_year)], sep = "," )
 
  
  # Convert Latitude and Longitude from DMS to DD ------------------------------  
  df <- separate(df, Latitude, into = c("Latitude_DD", "Longitude_DD"), 
                 sep = ",", remove = TRUE)

  # Rename columns (all names need to be moved over one because 
  # latitude and longitude are both under the latitude column 
  colnames(df) <- c("Time", "Latitude_DD", "Longitude_DD", "Altitude")
  df <- df[!is.na(names(df))] # Remove "NA" column

  # Prepare latitude for conversion by removing N and '
  df$Latitude_DD <- gsub("'|N", " ", df$Latitude_DD)
  df$Longitude_DD <- gsub("'|W", " ", df$Longitude_DD)

  # Remove leading and trailing whitespace from character strings
  df$Latitude_DD <- trimws(df$Latitude_DD)
  df$Longitude_DD <- trimws(df$Longitude_DD)

  # Replace nth character where degree symbol should be with a space 
  substring(df$Latitude_DD, 3, 3) <- " "
  substring(df$Longitude_DD, 4, 4) <- " "
  
  # Make longitude negative
  df$Longitude_DD <- paste("-", df$Longitude_DD, sep = "") 
  
  # Convert DDM latitude and longitude DMS to DD
  df$Latitude_DD <- conv_unit(df$Latitude_DD, from = "deg_min_sec", to = "dec_deg" )
  df$Longitude_DD <- conv_unit(df$Longitude_DD, from = "deg_min_sec", to = "dec_deg")
  
  
  # Clean up Date and Time fields ---------------------------------------------- 
  df$DateTime <- as.POSIXct(df$Time, format = "%d/%m/%Y %H:%M", tz = "GMT" )
  df$DateTime_PST <- df$DateTime
  attr(df$DateTime_PST, "tzone") <- "America/Vancouver"
  
  # Add "T" between Date and Time to separate columns
  df$DateTime_PST_T <- gsub(" ", "T", paste(df$DateTime_PST))
  
  # Separate DateTime_PT column into separate columns for Year (YYYY), 
  # Month (mm), Day (dd), Hour (HH), Minute (MM), Second (SS)
  df <- separate(df, DateTime_PST_T, into = c("Year_PST", "Month_PST", "DayTime"), 
                 sep = "-", remove = TRUE)
  df <- separate(df, DayTime, into = c("Day_PST", "Time_PST"), sep = "T", remove = TRUE)
  df <- separate(df, Time_PST, into = c("Hour_PST", "Min_PST", "Sec_PST"), 
                 sep = ":", remove = FALSE)
  
  # Add Mission name and number 
  df$Mission <- mission
  df$MissionNum <- mission.number
  
  # Reorder columns
  final.df <- df[ ,c("Mission", "MissionNum", "Latitude_DD", "Longitude_DD", "Altitude",
                     "DateTime", "DateTime_PST", "Year_PST", "Month_PST",
                     "Day_PST", "Time_PST", "Hour_PST", "Min_PST", "Sec_PST")]
  
  # Write aircraft tracks flyover by Mission date
  write.csv(final.df, paste0("G:/CP_ASP_flyovers/data/", f, "AircraftTracks", ".csv"), row.names = FALSE) 

  
  # Create .csv with column names to append aircraft tracks 
  # Only need to do this the first time. Otherwise there will be no column names 
  # in the appended csv
  
  data.dirs <- list.dirs("G:/CP_ASP_flyovers/data")
  
  append.df <- list.files(data.dirs, pattern = "AllAircraft*", full.names = TRUE)
  
  if (length(append.df > 0)) {
    print("The appended aircraft tracks file is already there.")
  } else {
    print("Create empty aircraft tracks csv.")
    df.append <- data.frame(matrix(ncol = ncol(final.df), nrow = 0))
    x <- colnames(final.df)
    colnames(df.append) <- x
    write.csv(df.append, "G:/CP_ASP_flyovers/data/AllAircraftTracks.csv", row.names = FALSE)
  }
  
# Append all mission observations to existing AllMissionObservations.csv file 
write.table(final.df, "G:/CP_ASP_flyovers/data/AllAircraftTracks.csv", 
              append = TRUE, sep = ",", col.names = FALSE, row.names = FALSE)  
}

